module.exports = function nuxtTypography () {
  this.addPlugin({
    src: path.resolve(__dirname, 'plugin.js'),
    ssr: false,
    fileName: 'typography.js'
  })
}

module.exports.meta = require('./package.json')
