import Typography from 'typography'
import githubTheme from 'typography-theme-github'

export default function () {
  githubTheme.baseFontSize = '18px'
  const typography = new Typography(githubTheme)
  return typography.injectStyles()
}
